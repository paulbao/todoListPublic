import React from 'react';
import { ApolloProvider } from 'react-apollo';

import apolloClient from '../helpers/connection';
import Router from '../navigation/router';

export default () => (
  <ApolloProvider client={apolloClient}>
    <Router />
  </ApolloProvider>
);
