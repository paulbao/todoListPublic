import { shallow } from 'enzyme';
import React from 'react';

import TodoCell from './TodoCell';

it('should render TodoCell component', () => {
  const TodoData = { title: 'foo' };
  const wrapper = shallow(<TodoCell TodoData={TodoData} />);
  expect(wrapper).toMatchSnapshot();
});
