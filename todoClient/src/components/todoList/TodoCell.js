import React, { PureComponent } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const styles = StyleSheet.create({
  cellWrapper: {
    alignItems: 'center',
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  title: {
    fontSize: 20,
  },
});

class TodoCell extends PureComponent {
  onCellPress = () => {
    const { onPress, TodoData } = this.props;

    onPress({ TodoData });
  };

  render() {
    const { TodoData } = this.props;
    const { title } = TodoData || {};

    return (
      <TouchableOpacity onPress={this.onCellPress}>
        <View style={styles.cellWrapper}>
          <Text style={styles.title}>{title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default TodoCell;
