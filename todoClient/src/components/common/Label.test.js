import { shallow } from 'enzyme';
import React from 'react';

import Label from './Label';

it('should render normal Label component', () => {
  const wrapper = shallow(<Label>Foo</Label>);
  expect(wrapper).toMatchSnapshot();
});

it('should render error Label component with isError prop', () => {
  const wrapper = shallow(<Label isError>Foo</Label>);
  expect(wrapper).toMatchSnapshot();
});
