import { shallow } from 'enzyme';
import React from 'react';

import Button from './Button';

it('should render Button component', () => {
  const wrapper = shallow(<Button>Foo</Button>);
  expect(wrapper).toMatchSnapshot();
});
