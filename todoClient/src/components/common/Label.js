import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
  label: {
    fontSize: 24,
    paddingHorizontal: 15,
    paddingVertical: 10,
    fontWeight: '700',
  },
});

function Label({ label, isError }) {
  return (
    <View>
      <Text style={[styles.label, { color: isError ? 'red' : null }]}>{label}</Text>
    </View>
  );
}

export default Label;
