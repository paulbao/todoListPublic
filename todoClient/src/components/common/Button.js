import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
  buttonWrapper: {
    alignSelf: 'stretch',
    borderColor: '#000',
    borderRadius: 5,
    borderWidth: 1,
  },
  buttonLabel: {
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '600',
    paddingVertical: 20,
  },
});

const Button = props => {
  const { style, color, children } = props;

  return (
    <TouchableOpacity style={[styles.buttonWrapper, style]} {...props}>
      <Text style={[styles.buttonLabel, { color }]}>{children}</Text>
    </TouchableOpacity>
  );
};

export default Button;
