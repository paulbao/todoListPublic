import { StackNavigator } from 'react-navigation';

import NewTodoScreen from '../screens/NewTodoScreen';
import TodoListScreen from '../screens/TodoListScreen';
import TodoDetailScreen from '../screens/TodoDetailScreen';

const Router = StackNavigator({
  TodoList: {
    screen: TodoListScreen,
    navigationOptions: {
      title: 'Todo List',
    },
  },
  TodoDetail: {
    screen: TodoDetailScreen,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.title.toUpperCase()}`,
    }),
  },
  NewTodo: {
    screen: NewTodoScreen,
    navigationOptions: {
      title: 'New',
    },
  },
});

export default Router;
