import React, { PureComponent } from 'react';
import { ActivityIndicator, FlatList, StyleSheet, View } from 'react-native';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import Button from '../../components/common/Button';
import Label from '../../components/common/Label';
import TodoCell from '../../components/todoList/TodoCell';

const styles = StyleSheet.create({
  listWrapper: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  button: {
    margin: 20,
  },
});

class TodoListContainer extends PureComponent {
  keyExtractor = item => item._id;

  onTodoPress = ({ TodoData }) => {
    const { navigation } = this.props || {};
    navigation.navigate('TodoDetail', { ...TodoData });
  };

  renderTodoCell = ({ item }) => !item.done && <TodoCell onPress={this.onTodoPress} TodoData={item} />;

  onAddClicked = () => {
    const { navigation } = this.props || {};
    navigation.navigate('NewTodo');
  };

  render() {
    const { data } = this.props || {};
    const { error, loading, todos } = data || {};

    if (error) {
      return <Label isError text="An unexpected error occurred" />;
    }

    if (loading) {
      return <ActivityIndicator />;
    }

    return (
      <View style={styles.listWrapper}>
        <FlatList data={todos} keyExtractor={this.keyExtractor} renderItem={this.renderTodoCell} />
        <View style={styles.button}>
          <Button onPress={this.onAddClicked}>Add Todo</Button>
        </View>
      </View>
    );
  }
}

export const TodoListQuery = gql`
  query TodoListQuery {
    todos {
      _id
      title
      done
    }
  }
`;

const TodoListWithData = graphql(TodoListQuery)(TodoListContainer);

export default TodoListWithData;
export { TodoListContainer };
