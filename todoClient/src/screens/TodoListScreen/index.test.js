import { shallow } from 'enzyme';
import React from 'react';

import { TodoListContainer } from './';

describe('screens/TodoListScreen', () => {
  it('should render empty list without data', () => {
    const wrapper = shallow(<TodoListContainer />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render error Label with error', () => {
    const data = { error: 'foo' };
    const wrapper = shallow(<TodoListContainer data={data} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render ActivityIndicator while loading', () => {
    const data = { loading: true };
    const wrapper = shallow(<TodoListContainer data={data} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render Todo List with Todos data', () => {
    const data = { todos: [{ _id: '1', title: 'foo', content: 'bar' }] };
    const wrapper = shallow(<TodoListContainer data={data} />);
    expect(wrapper).toMatchSnapshot();
  });
});
