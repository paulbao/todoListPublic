import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import gql from 'graphql-tag';
import { compose, graphql } from 'react-apollo';

import Button from '../../components/common/Button';
import Label from '../../components/common/Label';
import { TodoListQuery } from '../TodoListScreen';

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'space-around',
  },
  button: {
    padding: 15,
  },
});

const TodoDetail = ({ data = {}, mutate, navigation }) => {
  const { error, loading, todo } = data;
  if (error) {
    return <Label isError text="An unexpected error occurred" />;
  }

  if (loading) {
    return <ActivityIndicator />;
  }

  if (!todo) {
    return <Label isError text="Todo does not have a detail" />;
  }

  return (
    <View style={styles.wrapper}>
      <Label label={todo.content} />
      <View style={styles.button}>
        <Button
          onPress={() => {
            mutate({
              variables: {
                todo: {
                  _id: navigation.state.params._id,
                },
              },
              refetchQueries: [{ query: TodoListQuery }],
            }).then(() => {
              navigation.goBack();
            });
          }}
        >
          Mark as done
        </Button>
      </View>
    </View>
  );
};

const TodoDetailQuery = gql`
  query TodoDetailQuery($todoID: String!) {
    todo(id: $todoID) {
      title
      content
    }
  }
`;
const MarkAsDoneMutation = gql`
  mutation markAsDone($todo: TodoIDInput!) {
    markAsDone(input: $todo) {
      _id
    }
  }
`;

const TodoDetailWithData = compose(
  graphql(TodoDetailQuery, {
    options: props => ({
      variables: { todoID: props.navigation.state.params._id },
    }),
  }),
  graphql(MarkAsDoneMutation, {
    options: props => ({
      variables: { todo: props.navigation.state.params._id },
    }),
  })
)(TodoDetail);

export { TodoDetail };
export default TodoDetailWithData;
