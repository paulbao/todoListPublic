import { shallow } from 'enzyme';
import React from 'react';

import { TodoDetail } from './';

describe('screens/TodoDetailScreen', () => {
  it('should render error Label without data', () => {
    const wrapper = shallow(<TodoDetail />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render error Label with error', () => {
    const data = { error: 'foo' };
    const wrapper = shallow(<TodoDetail data={data} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render ActivityIndicator while loading', () => {
    const data = { loading: true };
    const wrapper = shallow(<TodoDetail data={data} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render Todo Detail with Todo data', () => {
    const data = { todo: { content: 'bar' } };
    const wrapper = shallow(<TodoDetail data={data} />);
    expect(wrapper).toMatchSnapshot();
  });
});
