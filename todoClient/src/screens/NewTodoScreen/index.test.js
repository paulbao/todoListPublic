import { shallow } from 'enzyme';
import React from 'react';

import { NewTodoContainer } from './';

describe('screens/NewTodoScreen', () => {
  it('should render without title and content', () => {
    const wrapper = shallow(<NewTodoContainer />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render with title and content', () => {
    const wrapper = shallow(<NewTodoContainer />);
    wrapper.setState({ title: 'foo', content: 'bar' });
    expect(wrapper).toMatchSnapshot();
  });
});
