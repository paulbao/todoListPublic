import React, { PureComponent } from 'react';
import { StyleSheet, View, TextInput, Text } from 'react-native';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';

import Button from '../../components/common/Button';
import { TodoListQuery } from '../TodoListScreen';

const styles = StyleSheet.create({
  wrapper: {
    marginTop: 30,
    margin: 15,
  },
  titleEditor: {
    height: 40,
    marginVertical: 15,
    borderColor: 'gray',
    borderWidth: 1,
  },
  contentEditor: {
    marginVertical: 15,
    height: 200,
    borderColor: 'gray',
    borderWidth: 1,
  },
});

class NewTodoContainer extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      title: null,
      content: null,
    };
  }

  onChangeContent = content => {
    this.setState({ content });
  };

  onChangeTitle = title => {
    this.setState({ title });
  };

  onSaveClicked = () => {
    const { mutate, navigation } = this.props;
    const { title, content } = this.state;
    mutate({
      variables: {
        todo: {
          title,
          content,
        },
      },
      refetchQueries: [{ query: TodoListQuery }],
    }).then(() => {
      navigation.goBack();
    });
  };

  render() {
    const { title, content } = this.state;

    return (
      <View style={styles.wrapper}>
        <Text>Title:</Text>
        <TextInput onChangeText={this.onChangeTitle} style={styles.titleEditor} value={title} />
        <Text>Content:</Text>
        <TextInput multiline onChangeText={this.onChangeContent} style={styles.contentEditor} value={content} />
        <Button onPress={this.onSaveClicked}>Save</Button>
      </View>
    );
  }
}

const NewTodoMutation = gql`
  mutation addTodo($todo: TodoInput!) {
    addTodo(input: $todo) {
      _id
    }
  }
`;

const NewTodoWithMutation = graphql(NewTodoMutation)(NewTodoContainer);

export default NewTodoWithMutation;
export { NewTodoContainer };
