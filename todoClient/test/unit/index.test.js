import React from 'react';
import { shallow } from 'enzyme';

import App from '../../src/app';

test('renders app', () => {
  const component = shallow(<App />);
  expect(component).toMatchSnapshot();
});
