# Running the application
The repository is divided into two parts:
  * todoServer

  * todoClient

To run the server you have to make sure that `mongod` is running!!

## To run the server:
From the root director:

`cd server`

`yarn`

`yarn start`

If everything went good you should see following:

`GraphQL Server is now running on http://localhost:4000
Connected to DB`

## To run the client:
From the root directory:

`cd client`

`yarn`

After installation run following:

`react-native start`

this will start the react-native packager which is needed for running the app. Open new tab in your terminal, from root folder navigate to following:

`cd client`

and finally run

`react-native run-ios`
