export const typeDefs = `
  type Query {
   todos: [Todo]
   todo(id: String!): Todo
  }
  type Todo {
    _id: ID!
    title: String
    content: String
    done: Boolean
  }
  input TodoInput {
    title: String
    content: String
  }

  input TodoIDInput {
    _id: ID!
  }
  input IngredientInput {
    name: String
    amount: Int
    amountUnit: String
  }

  type Mutation {
    addTodo(input: TodoInput): Todo
    markAsDone(input: TodoIDInput): Todo
  }
`;
