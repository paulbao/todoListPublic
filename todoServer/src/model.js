import mongoose, { Schema } from 'mongoose';

const todoSchema = new Schema({
  title: String,
  content: String,
  done: Boolean,
});
export const todoModel = mongoose.model('todo', todoSchema);
