import { db } from './conectors';
import { todoModel } from './model';

export const resolvers = {
  Query: {
    todos: () => {
      return db.model('todo').find().then(todos =>
        todos.map(todo => todo)
      );
    },
    todo: (_, { id }) => {
      return db.model('todo').findOne({ "_id": id }).then(todo => todo);
    },
  },

  Mutation: {
    addTodo: (root, args) => {
      const { input } = args || {};
      const todo = new todoModel({ ...input });

      todo.save().then(newTodo => {
        console.log('saved', newTodo)
      }).catch((err) => {
        console.log('err', err)
      });

      return todo;
    },
    markAsDone: (root, args) => {
      const { input } = args || {};
      return db.model('todo').findOne({ "_id": input._id }).then(todo => {
        todo.done = true;
        todo.save().then(newTodo => {
          console.log('saved', newTodo)
        }).catch((err) => {
          console.log('err', err)
        });
  
        return todo;
      });
    },
  },
};
