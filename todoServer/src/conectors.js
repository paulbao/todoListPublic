import bluebird from 'bluebird';
import mongoose from 'mongoose';

mongoose.Promise = bluebird;
mongoose.connect('mongodb://localhost/receptar');

const db = mongoose.connection;

db.on('error', () => console.log('Failed to connect to DB'));
db.on('close', () => console.log('Connection closed'));
db.on('open', () => console.log('Successfully connected to DB!'));

export { db };
